Generic Bucket for static websites blueprint
===============================

This module has the intent to provide a S3 bucket to provide a static website or a site assets via HTTPS.

# Use

To create a bucket with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source = "git@bitbucket.org:credibilit/terraform-s3-static-website-blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

**Note**: Bucket policies must be enabled by the [s3_bucket_policy resource](https://www.terraform.io/docs/providers/aws/r/s3_bucket_policy.html).

## Input Parameters

The following parameters are used on this module:

- **account**: The AWS account number ID.

## Output parameters

The following outputs can be accessed by the module which call this one.

- **id**: The bucket id.
- **arn**: The AWS ARN bucket reference.
- **hosted_zone_id**: The Route53 hosted zone id which the bucket is registered.
- **region**: The region which the bucket was created.
