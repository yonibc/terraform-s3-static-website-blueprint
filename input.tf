variable "account" {
  description = "The AWS account number ID"
}

variable "bucket_name" {
  description = "The bucket name, ensure its globally unique"
  type = "string"
}

variable "acl" {
  description = "The S3 bucket 'Canned ACL' (https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl)"
  type = "string"
  default = "private"
}

variable "force_destroy" {
  description = "If the bucket must destroy all objects inside the bucket before delete the bucket (default: false)"
  type = "string"
  default = false
}

variable "versioning_enabled" {
  description = "If this bucket must versioning the objects. (default: false)"
  type = "string"
  default = false
}

variable "index_document" {
  type = "string"
  description = "The index document to point when accessing the root key or subkeys"
  default = "index.html"
}

variable "error_document" {
  type = "string"
  description = "The error page to show when erros occurs"
  default = "error.html"
}

variable "routing_rules" {
  type = "string"
  description = "A json array containing routing rules describing redirect behavior and when redirects are applied"
  default = ""
}
